#!/usr/bin/env python3

from pygame.locals import *
from random import choice, randint
from time import time
import pygame

letters = "abcdefghijklmnopqrstuvwxyz' "
SAFE = "unvraimotdepasseaucasou"
path = '/home/hatrix/git/wordlock/'
before = ''
after = ''

def display(current, word, correct):
    screen.fill(background)
    w = font.render(word[0], 1, foreground)
    screen.blit(w, (size[0]/2 - font.size(word[0])[0]/2, size[1]/3))

    cu = '*' * len(current) # **************
    cu = current
    c = font.render(cu, 1, foreground)
    screen.blit(c, (size[0]/2 - font.size(cu)[0]/2, size[1]/2))

    c = '*' * correct[0] + (correct[1] - correct[0]) * '-'
    c = font.render(c, 1, foreground)
    screen.blit(c, (20, 20))

    pygame.display.flip()

def change_word():
    r = randint(0, 1)
    l = list(map(str.strip, choice(words).strip().split(',')))
    if r:
        return l[::-1]
    return l

def loop(current, word, correct):
    go = True
    t = time()
    tt = time()
    while go:
        pygame.time.Clock().tick(60)

        if time() - t > 5:
            word = change_word()
            current = ""
            display(current, word, correct)
            t = time()

            
        event = pygame.event.poll()
        if event.type == QUIT:
            go = False

        if event.type == KEYDOWN:
            if event.unicode in letters:
                current += event.unicode
                display(current, word, correct)

            elif event.key == K_BACKSPACE:
                current = current[:-1]
                display(current, word, correct)

            elif event.key == K_RETURN:
                if current == before + word[1] + after or current == SAFE:
                    current = ''
                    correct[0] += 1
                    t = time()

                    if correct[0] >= correct[1]:
                        exit()
                    else:
                        word = change_word()
                    display(current, word, correct)


pygame.init()
pygame.key.set_repeat(1, 100)
pygame.mouse.set_visible(False)
infos = pygame.display.Info()
size = infos.current_w, infos.current_h
foreground = 238, 238, 238
background = 18, 18, 18
current = ''

screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
screen.fill(background)
font = pygame.font.Font(None, 36)

words = open(path + 'words.txt').readlines()
letters += ''.join(map(str.strip, open(path + 'words.txt').readlines()))
letters = list(set(letters))

correct = [0, 3]
word = change_word()
display(current, word, correct)
loop(current, word, correct)
